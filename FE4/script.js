var clickedUpload = document.getElementById("upload-dd-container");
var clickedCreate = document.getElementById("create-dd-container");
var clickedFave = document.getElementById("heartMe");
var favedColor = "invert(42%) sepia(10%) saturate(4169%) hue-rotate(232deg) brightness(86%) contrast(82%)";

function openUpload() {
    clickedUpload.style.visibility = "visible";
}
        
function cancelUpload() {
    clickedUpload.style.visibility = "hidden";
}

function openCreate() {
    clickedCreate.style.visibility = "visible";
}
        
function cancelCreate() {
    clickedCreate.style.visibility = "hidden";
}

function addTofave() {
    clickedFave.style.filter = favedColor;
}

function showFavorite() {
    document.getElementById("main-header").innerHTML = "FAVORITE TRACKS";
}